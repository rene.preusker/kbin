'''
Specific extractor of kbin coefficients for momo

usage 
see examples in test dir
'''
import sys

from lib import kbin_io
from lib import kbin_tools
from lib import kbin_deco
deco = kbin_deco.time_decorator

USAGE=__doc__
DESCR="KBIN"


@deco
def main():
    if len(sys.argv) != 2:
        print(USAGE)
        sys.exit(128)

    input_file = sys.argv[1]
    k2m_input = kbin_io.k2m_read_and_analyze_input(input_file)
    kbin_data = kbin_io.k2m_read_data(k2m_input)
    dum = kbin_tools.k2m_analyze_vtp(kbin_data, k2m_input)
    dum = kbin_io.k2m_write_kb_file(kbin_data, k2m_input)


if __name__=='__main__':
    main()
