import sys
sys.path.append('/all_mounts/masp20/nilma/CLARA/momo45_broadband/latest_kbin/kbin/kbin/lib/')
import numpy as np
# import kbin_deco
from . import kbin_deco


def ivm_to_weights(ivm,rsr):
    '''
    Calculates weights from _mapping
    '''
    nnn = int(ivm.max())+1 # number of weights 
    wgt = np.zeros(nnn)
    for i in range(nnn):
        idx = (ivm == i)
        wgt[i] = rsr[idx].sum()
    wgt /= wgt.sum()
    return wgt


def opt_to_trans(opt,rsr=None,wvl=None,amf=1):
    '''
    transmission from optical thickness
    '''
    if rsr is None:
        return np.exp(-opt*amf).sum()/len(opt)
    else:
        return np.trapz(np.exp(-opt*amf)*rsr,wvl)
    
def calc_ave_opt(wgts,mapp,opd,method=None):
    '''
    calculates the average  extinction (actually optical thickness ...)
    from given weight and mapping 
    '''
    ave_opt = np.zeros_like(wgts)
    for iwgt,wgt in enumerate(wgts):
        idx = (mapp == iwgt)
        if method in ('tra','trans'):
            ave_opt[iwgt] = -np.log(np.exp(-opd[idx]).mean())
        elif method in('opd','opt'):
            ave_opt[iwgt] = opd[idx].mean()
        else:
            eee='Unknown averaging method: %s'%method
            kbin_deco.problem_print(eee)
            sys.exit(128)

    return ave_opt

def ave_opt_to_trans(ave_opt,wgt,amf):
    return (np.exp(-ave_opt*amf)*wgt).sum()

def ave_opt_to_trans_amf(ave_opt,wgt,amf):
    return np.array([ave_opt_to_trans(ave_opt,wgt,a) for a in amf])

def calc_all_ave_opt(kb, method,types):
    '''
    wrapper around calc_ave_opt
    '''
    def doit(opd):
        return calc_ave_opt(kb['weight'],
                            kb['mapping'],
                            opd, 
                            method =method)
        
    for typ in types:
        kb['lay_ave_opt_%s'%typ] = np.zeros((kb['n_weight'],kb['n_lay']))
        kb['ttl_ave_opt_%s'%typ] = np.zeros((kb['n_weight'],kb['n_lay']))
        
        kb['toa_ave_opt_%s'%typ] = doit(kb['toa_opt_%s'%typ])
        for ilay in range(kb['n_lay']):
            kb['lay_ave_opt_%s'%typ][:,ilay] = doit(kb['lay_opt_%s'%typ][:,ilay])
            kb['ttl_ave_opt_%s'%typ][:,ilay] = doit(kb['ttl_opt_%s'%typ][:,ilay])

def calc_all_ave_trans(kb,types):
    '''
    wrapper around ave_opt_to_trans
    however, all is done with the lay_ave_opt_???
    since this will be  the basis for the radiative 
    transfer later!!!
    '''
    for typ in types:
        ttl_ave_opt = kb['lay_ave_opt_%s'%typ].cumsum(axis=1)
        lay_ave_opt = kb['lay_ave_opt_%s'%typ]
        ttl_ave_tra = np.zeros((kb['n_lay'],kb['n_amf']))
        lay_ave_tra = np.zeros((kb['n_lay'],kb['n_amf']))
        for ilay in range(kb['n_lay']):
            for iamf,amf in enumerate(kb['amf']):
                ttl_ave_tra[ilay,iamf] = ave_opt_to_trans(ttl_ave_opt[:,ilay],kb['weight'],amf)
                lay_ave_tra[ilay,iamf] = ave_opt_to_trans(lay_ave_opt[:,ilay],kb['weight'],amf)
        kb['ttl_ave_trans_%s'%typ]=ttl_ave_tra
        kb['lay_ave_trans_%s'%typ]=lay_ave_tra
        kb['toa_ave_trans_%s'%typ]=ttl_ave_tra[-1,:]

# def calc_err(w,m,opt,tra,amf,met):
#     ave_opt = calc_ave_opt(w,m,opt,met)
#     ave_tra = ave_opt_to_trans_amf(ave_opt,w,amf)
#     e = np.abs(ave_tra - tra).max()
#     return e


def calc_err(wgts,mapp,opt,tra,amf,met,err_meas='mean_err'):
    import matplotlib.pylab as plt # TODO: clean up an dtest
    #plt.ion()
    #plt.close('all')
    ave_opt = calc_ave_opt(wgts,mapp,opt,met) # shape: (nbins)
    print(ave_opt.shape)
    ave_tra = ave_opt_to_trans_amf(ave_opt,wgts,amf) # sahpe: (n_amf)
    print(wgts.shape)
    if err_meas == 'mean_err':
        e = np.abs(ave_tra - tra).max()
    elif err_meas == 'max_err':
        bin_err = np.zeros_like(wgts)
        for iwgt,wgt in enumerate(wgts):
            idx = (mapp == iwgt)
            #hr_trans = np.exp(-opt[idx]*a)
            bin_err[iwgt] = np.array([np.exp(-ave_opt[iwgt]*a)*wgt-np.exp(-opt[idx]*a).sum()*wgt for a,i in enumerate(amf)]).max()
            # for i,a in enumerate(amf):
            #     hr_trans = np.exp(-opt[idx]*a)
            #     #diff_trans = ave_tra[i] - hr_trans
            #     diff_trans = np.exp(-ave_opt[iwgt]*a) - hr_trans#.mean()
            #     if (i<3) and (iwgt<3):
            #         print(diff_trans)
            #     #     plt.figure()
            #     #     plt.plot(hr_trans)
            #     #     plt.figure()
            #     #     plt.plot(diff_trans, label=f'iwgt{iwgt}, amf{a}')
            #     #     plt.legend()
            #     #     plt.savefig(f'/home/nilma/Nextcloud/nilma/tmp/wgt{iwgt}ams{a}.png')
            #     max_err[iwgt] = diff_trans.max()
        e = np.abs(bin_err).max()

    else: 
        eee='Unknown err_meas method: %s'%err_meas
        kbin_deco.problem_print(eee)
        sys.exit(128)
    return e

def combine_mappings(m1,m2): # NOTE N.M. not beeing used anymore!!?
    '''
    this combines mappings m1 and m2
    if m1 and m2 
    '''
    m3 = np.zeros_like(m1)
    
    i3 = 0 
    for i1 in range(m1.max()+1):
        for i2 in range(m2.max()+1):
            idx = (m1 == i1) & (m2 == i2)
            if idx.sum() > 0:
                m3[idx] = i3
                i3 += 1
    return m3

def find_bin_with_largest_spread(opt,mpp):
    val = np.log(0.01 + opt)
    std = 0.
    for iii in range(mpp.max()+1):
        idx = (mpp == iii)
        if idx.sum() >= 3:
            dum = val[idx].std()
            if std < dum:
                ooo, std = iii,dum
    return ooo


def k_sort(opt, rsr, nnn, sort_methods, tra=None, amf=None, average_method=None):
    if type(sort_methods) is str:
        return k_s_sort(opt,rsr,nnn,sort_methods)
    if len(sort_methods) == 1:
        return k_s_sort(opt,rsr,nnn,sort_methods[0])
    
    w_m = [k_s_sort(opt,rsr,nnn,m) for m in sort_methods]
    
    # If more than one sorting method is given, I need 
    # a measure to decide, which one to use. Up to now 
    e = [calc_err(w,m,opt,tra,amf,average_method) for w,m in w_m] 
    best_index = e.index(min(e))
    #print('Using: ',sort_methods[best_index])
    return w_m[best_index]
    
def k_s_sort(opt,rsr,nnn,method):
    '''
    *The* heart of the k-binning 
    '''
    
    if method == 'rank':
        val = opt.argsort().argsort().astype(float)
        val = val/len(val)*(nnn)
    elif method == 'lnopt':
        val = np.log(0.01 + opt)
        mi,ma = val.min(),val.max()
        val = (val-mi)/(ma-mi)*nnn
    elif method == 'trans':
        val = np.exp(-opt)
        mi,ma = val.min(),val.max()
        val = (val-mi)/(ma-mi)*nnn
    else:
        eee='Error: Unknown sorting method: "'+method+'"' 
        kbin_deco.problem_print(eee)
        sys.exit(128)
        
    inverse_mapping = np.zeros_like(val,dtype=int)-1
    for i in range(nnn):
        idx = (i <= val) & (val <= (i+1)) 
        inverse_mapping[idx] = i
    weights = ivm_to_weights(inverse_mapping,rsr)

    # this should be commented somewhen,
    # if it nvere happens. It takes time! 
    if ((inverse_mapping == -1 ).sum()) > 0:
        eee = 'Not all hr wavelength have been mapped.\n'
        eee += 'There may be a problem' 
        kbin_deco.problem_print(eee)
    return weights,inverse_mapping


def k2m_analyze_vtp(kbin_data, k2m_input):
    '''
    analzes the momo vtp,
    in particular the cgs (= cgasa) section
    up to now just simple consistency check
    :param vtp:
    :return: None
    ::::return: struct with some numbers
    '''
    for gas in ['H2O', 'MGAS', 'O3']:
        gs = 'scale_%s' % gas
        if k2m_input['SCALE'][gs].size == 1:
            k2m_input['SCALE'][gs] = np.zeros(kbin_data['kb']['n_lay']) + k2m_input['SCALE'][gs][0]
        if k2m_input['SCALE'][gs].size != kbin_data['kb']['n_lay']:
            eee=(('Error: "%s" must has either 1 or %i elements, but has %i.'
                  % (gs,kbin_data['kb']['n_lay'],k2m_input['SCALE'][gs].size)) )
            kbin_deco.problem_print(eee)
            sys.exit(128)

    # vtp = kbin_data['vtp']
    # print(vtp.keys)
    # pass

    
    
    
    
    

