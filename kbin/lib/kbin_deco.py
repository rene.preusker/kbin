import time
from termcolor import colored
import inspect


 
def time_decorator(function):
    def decorated(*args, **kwargs):
        ts = time.time()
        result = function(*args, **kwargs)
        te = time.time()
        print("{0}:  {1:.2} sec".format(function.__name__, te - ts))
        return result
    return decorated

def debug_decorator(function):
    def decorated(*args, **kwargs):
        ts = time.asctime()
        print("{0}: {1}({2}, {3}) started"
              .format(ts,function.__name__, args, kwargs))
        result = function(*args, **kwargs)
        te = time.asctime()
        print("{0}: {1}) finished"
              .format(te,function.__name__))
        return result
    return decorated

def none_decorator(function):
    return function


def except_decorator(function):
    def decorated(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except Exception as  e:
            print ("Got error! ", repr(e))
            return None
    return decorated

def problem_print(eee):
    txt = colored(''.join(('There is a problem in: ',inspect.stack()[1][3])),'red')
    print(txt)
    for e in inspect.stack()[1:]:
        print('-')
        for ee in e:
            print('  ',ee)
    #print(inspect.stack())
    print(eee)

