import sys
import numpy as np
from . import kbin_tools
from . import kbin_deco

# import kbin_tools
# import kbin_deco

deco = kbin_deco.time_decorator
#deco = kbin_deco.debug_decorator

def kbin_mean(kbin_data,kbin_input):
    '''
    bla
    '''
    kb = kbin_data['kb']
    return np.ones(1),np.zeros_like(kb['wvl'])

def correlated_toa(kbin_data,kbin_input):
    '''
    bla
    '''
    kb = kbin_data['kb']
    kc = kbin_input['CONFIGURATION'] 
    ki = kbin_input['INTERNALS'] 
    opt = kb['toa_opt_tot']
    tra = kb['toa_trans_tot']
    w, m = kbin_tools.k_sort(opt, kb['rsr'], kc['max_bins'], ki['sort_over'],
                             tra, kb['amf'], ki['average_method'])
    return w, m 

def correlated_strongest_layer(kbin_data,kbin_input):
    '''
    strongest abs = min trans
    '''
    kb = kbin_data['kb']
    kc = kbin_input['CONFIGURATION']
    ki = kbin_input['INTERNALS'] 
    strong_idx=kb['lay_trans_tot'][:,1].argmin()
    opt = kb['lay_opt_tot'][:,strong_idx]
    tra = kb['lay_trans_tot'][strong_idx,:]

    w, m = kbin_tools.k_sort(opt, kb['rsr'], kc['max_bins'], ki['sort_over'],
                             tra, kb['amf'], ki['average_method'])
    return w, m 

def correlated_toa_strongest_layer(kbin_data, kbin_input):
    '''
    strongest abs = min trans TODO: optional percentage of toa and strongest layer --> Bennartz 2000
    '''
    kb = kbin_data['kb']
    kc = kbin_input['CONFIGURATION']
    ki = kbin_input['INTERNALS'] 
    
    strong_idx=kb['lay_trans_tot'][:,1].argmin()
    opt = kb['lay_opt_tot'][:,strong_idx]*(1.-ki['weight_toa']) + kb['toa_opt_tot']*ki['weight_toa']
    tra = kb['lay_trans_tot'][strong_idx,:]*(1.-ki['weight_toa']) + kb['toa_trans_tot']*ki['weight_toa']
    
    w, m = kbin_tools.k_sort(opt, kb['rsr'], kc['max_bins'], ki['sort_over'],
                             tra, kb['amf'], ki['average_method'])
    return w, m 

def analyse_layer(kbin_data, kbin_input, ilay, w, m):
    kb = kbin_data['kb']    
    kc = kbin_input['CONFIGURATION']
    ki = kbin_input['INTERNALS']
    if ilay == 'toa':
        opt = kb['toa_opt_tot']
        tra = kb['toa_trans_tot']
    else:
        opt = kb['lay_opt_tot'][:,ilay]
        tra = kb['lay_trans_tot'][ilay,:]
    return kbin_tools.calc_err(w,m,opt,tra,kb['amf'],ki['average_method'], ki['err_meas'])


def uncorrelated(kbin_data,kbin_input):
    '''
    w weights
    m combine_mappings
    e error
    '''
    kb = kbin_data['kb']    
    kc = kbin_input['CONFIGURATION']
    
    # initialize  with two bins in toa :
    w, m ,e = optimize_single_layer(kbin_data,kbin_input,'toa',2)
    
    # optimize toa!
    w, m ,e = improve_single_layer(kbin_data,kbin_input,'toa',w,m)
    # optimize all other layers, if needed!
    for ilay in range(kb['n_lay'])[::-1]:
        e = analyse_layer(kbin_data,kbin_input,ilay,w,m)
        # print('testing', ilay,e,m.max())
        if kc['max_err_lay'][ilay]  >= e:
            #print('already good',e,'<',kc['max_err_lay'][ilay])
            continue
        w, m ,e = improve_single_layer(kbin_data,kbin_input,ilay,w,m)
        # print('improving', ilay,e,m.max())
    
    e = analyse_layer(kbin_data,kbin_input,'toa',w,m)
    #print('testing toa',e,m.max())
    if kc['max_err_toa'] < e:
        w, m ,e = improve_single_layer(kbin_data,kbin_input,'toa',w,m)
        #print('improving toa', e,m.max())
    return w,m



def improve_single_layer(kbin_data,kbin_input,ilay,w,m):
    '''
    improve_single a single layer until 
    error is below max_error
    '''
    max_bins = kbin_input['CONFIGURATION']['max_bins']
    if ilay == 'toa':
        max_error = kbin_input['CONFIGURATION']['max_err_toa']
    else:
        max_error = kbin_input['CONFIGURATION']['max_err_lay'][ilay]
    
    while True:
        w, m ,e = identify_bin_with_largest_spread_and_split_it(kbin_data,kbin_input,ilay,m,w)
        n_bins = len(w)
        if n_bins >= max_bins:
            #print('maxbin break ',n_bins,max_bins)
            break
        if e < max_error:
            #print('max error break ',e,max_error)
            break

    return w, m ,e 


               

def identify_bin_with_largest_spread_and_split_it(kbin_data,kbin_input,ilay,mpp,wgt):
    kb = kbin_data['kb']
    kc = kbin_input['CONFIGURATION'] 
    ki = kbin_input['INTERNALS'] 
    
    if ilay == 'toa':
        opt = kb['toa_opt_tot']
        tra = kb['toa_trans_tot']
    else:
        opt = kb['lay_opt_tot'][:,ilay]
        tra = kb['lay_trans_tot'][ilay,:]

    # find badest_bin 
    b_bin = kbin_tools.find_bin_with_largest_spread(opt,mpp)

    # badest index
    b_idx = (mpp==b_bin)

    # extract all for that bin
    b_opt = opt[b_idx]+0.
    b_map = mpp[b_idx]+0
    b_rsr = kb['rsr'][b_idx]+0

    #divide badest bin into two new bins
    b_w,b_m= kbin_tools.k_sort(b_opt,b_rsr,2,ki['sort_over']
                               ,tra,kb['amf'],ki['average_method'])

    # b_m == 0, keeps old binning number 
    # b_m == 1, gets new binning number (len(wgt))
    b_map[b_m == 1] = len(wgt)
    
    # put it back into the old mapping
    mpp[b_idx] = b_map
 
    wgt = kbin_tools.ivm_to_weights(mpp,kb['rsr'])
    err = kbin_tools.calc_err(wgt,mpp,opt,tra,kb['amf'],ki['average_method'], ki['err_meas'])
    return wgt,mpp,err
        

def optimize_single_layer(kbin_data,kbin_input,ilay,max_bins=None):
    '''
    optimizes a single layer by increasing 
    number of bins until 
    error < below max_error
    or 
    nbins  > max_bins
    '''
    kb = kbin_data['kb']
    kc = kbin_input['CONFIGURATION']    
    ki = kbin_input['INTERNALS'] 

    # check if total atmosphere
    if ilay == 'toa':
        max_error = kc['max_err_toa']
        opt = kb['toa_opt_tot']
        tra = kb['toa_trans_tot']
    else:
        max_error = kc['max_err_lay'][ilay]
        opt = kb['toa_opt_tot']
        tra = kb['toa_trans_tot']

    if max_bins is None:
        max_bins = kc['max_bins']

    n_bins = 1
    while True:        
        w, m  = kbin_tools.k_sort(opt,kb['rsr'],n_bins,ki['sort_over']
                                ,tra,kb['amf'],ki['average_method']) 
        e = kbin_tools.calc_err(w,m,opt,tra,kb['amf'],ki['average_method'], ki['err_meas'])
        
        #print('having ',n_bins,e,max_error)
        if n_bins >= max_bins:
            #print('max bin break ',e,max_error)
            break
        if e < max_error:
            #print('max error break ',e,max_error)
            break
        n_bins +=1
    
    return w, m ,e 
    

