import os
import sys
import re
import time
import getpass
import inspect

import numpy as np

if sys.version_info >= (3,):
    import configparser
else:
    import ConfigParser as configparser

from netCDF4 import Dataset
from . import kbin_deco
# import kbin_deco

deco = kbin_deco.time_decorator
#deco = kbin_deco.debug_decorator
#deco = kbin_deco.none_decorator


AVOGADRO = 6.02214086e23  # mol-1

KBIN_NEEDED_INPUT_VARIABLES = {
    'FILES':
        {
            'cgasa_file': 's',
            'vtp_file': 's',
            'rsr_identifier': 's',
            'solar_file': 's',
            'output_file': 's',
            'detailed_output': 'b',
        },
    'CONFIGURATION':
        {
            'max_bins': 'i',
            'max_err_lay': 'lf',
            'max_err_toa': 'f',
        },
    'INTERNALS':
        {
            'min_amf': 'f',
            'max_amf': 'f',
            'nnn_amf': 'i',
            'self_cont': 'b',
            'frgn_cont': 'b',
            'kbin_method': 's',
            'average_method': 's',
            'sort_over': 's|ls',
            'err_meas': 's',
            'weight_toa': 'f'
        },
}
K2M_NEEDED_INPUT_VARIABLES = {
    'FILES':
        {
            'kbin_file': 's',
            'output_file_base': 's',
        },
    'SCALE':
        {
            'scale_H2O': 'lf',
            'scale_MGAS': 'lf',
            'scale_O3': 'lf',
        },
    'CONFIGURATION':
        {
            'verbosity': 'b'
        },
}
        
KBINOUT_FULL = ( 
          ('toa_opt', ('wvl',),'total_atmosphere_optical_thickness')
        , ('lay_opt', ('wvl', 'lay',),'layer_optical_thickness')
        , ('ttl_opt', ('wvl', 'lay',),'top_to_layer_optical_thickness')
        , ('toa_trans', ('amf',),'total_atmosphere_transmission')
        , ('lay_trans', ('lay', 'amf',),'layer_transmission')
        , ('ttl_trans', ('lay', 'amf',),'top_to_layer_transmission')
        , ('toa_ave_opt', ('weight',),'total_atmosphere_binaveraged_optical_thickness')
        , ('lay_ave_opt', ('weight', 'lay',),'layer_binaveraged_optical_thickness')
        , ('ttl_ave_opt', ('weight', 'lay',),'top_tolayer_binaveraged_optical_thickness')
        , ('toa_ave_trans', ('amf'),'total_atmosphere_kbin_transmission')
        , ('lay_ave_trans', ('lay', 'amf'),'layer_kbin_transmission')
        , ('ttl_ave_trans', ('lay', 'amf'),'top_to_layer_kbin_transmission')
        )

KBINOUT_SMALL = ( 
         ('lay_ave_opt', ('weight', 'lay',),'layer_binaveraged_optical_thickness'),
        )



# Python 2 / 3 inconsistency  
try:
    iteritems = dict.iteritems
except AttributeError:
    iteritems = dict.items


def ds2dict(ds):
    '''
    netcdf dataset variables -->  dict
    recursive!
    '''
    out = {}
    atr = {}
    for att in ds.ncattrs():
        atr[att] = ds.getncattr(att)
    out['attributes'] = atr
    for varn, varv in iteritems(ds.variables):
        out[varn] = varv[:] + 0.
    for grpn, grpv in iteritems(ds.groups):
        out[grpn] = ds2dict(grpv)
    return out


def simple_ncdf2dict(filename):
    with Dataset(filename) as ds:
        return ds2dict(ds)


#@deco
def kb_read_and_analyze_input(input_file):
    return read_and_analyze_input(input_file, KBIN_NEEDED_INPUT_VARIABLES)

def k2m_read_and_analyze_input(input_file):
    return read_and_analyze_input(input_file, K2M_NEEDED_INPUT_VARIABLES)


def read_and_analyze_input(input_file, rule):
    '''
    input file must be a valid kbin input/steering
          file in 'ini' style
    output is a dict conatining filename numbers etc 
          to control the kbin run
    '''
    if not os.path.isfile(input_file):
        eee = "Problem: '%s' is not found" % ( input_file)
        kbin_deco.problem_print(eee)
        sys.exit(128)

    try:
        config = config2dict(input_file)
    except Exception as eee:
        kbin_deco.problem_print(eee)
        sys.exit(128)

    for section in rule:
        if section not in config:
            eee = "Problem: Section '%s' is missing in inputfile '%s'" % (section, input_file)
            kbin_deco.problem_print(eee)
            sys.exit(128)
        for element in rule[section]:
            if element not in config[section]:
                eee = ("Problem: Section '%s' misses element '%s' in inputfile '%s'"
                       % (section, element, input_file))
                kbin_deco.problem_print(eee)
                sys.exit(128)
            else:
                config[section][element] = convert_type(config[section][element],
                                                        rule[section][element])
    return config


def is_float(f):
    try: _ = float(f)
    except ValueError:
        return False
    return True

def string2np(stg):
    dum = [float(_) for _ in stg.split(',') if is_float(_) ]
    return np.array(dum)

def convert_type(inn, typ):
    '''
    tiny comodity function for 
    input parsing and converting
    '''
    if typ == 'f':
        out = float(inn)
    elif typ == 's':
        out = inn
    elif typ == 'i':
        out = int(inn)
    elif typ == 'b':
        if inn.lower() in ("yes", "true", "t", "1"):
            out = True
        else:
            out = False
    elif typ == 'lf':
        out = string2np(inn)
    elif typ == 'ls':
        out = [i.strip() for i in inn.split(',')]
    elif typ == 's|ls':
        out = [i.strip() for i in inn.split(',')]
        if len(out) == 1: out = out[0]
    else:
        out = None
        print('Problem, ask Rene')
    return out


def config2dict(configfile):
    '''
    Small simple wrapper for Configparser. 
    makes a dict form the *.ini file
    '''
    models = configparser.ConfigParser()
    models.optionxform = str
    models.read(configfile)
    d = dict(models._sections)
    for k in d:
        d[k] = dict(d[k])
        d[k].pop('__name__', None)
    return d


def find_int(inn):
    '''
    find integers in strings,
    simple wraper around re
    '''
    xx = re.findall("[0-9]+", inn)
    if len(xx) > 0:
        return int(xx[-1])
    return None


@deco
def read_vtp_momo(vtp_file):
    '''
    This is a very very specific acii reader for 
    the file describing the vertical profile in the 
    momo universe. You may write  your own reader for your
    aplication. However: the output must be consistent!!!!!
    '''
    try:
        with open(vtp_file) as fp:
            d = fp.readlines()
    except Exception as eee:
        kbin_deco.problem_print(eee)
        sys.exit(128)

    # first two lines give the number of levels:
    ntot = find_int(d[0])
    natm = find_int(d[1])

    # First block LEVEL
    # lines 4... all levels
    # get the head of the table
    key = [x.split('(')[0] for x in d[3].strip()[1:-1].split()]
    lev = {}
    for ik, k in enumerate(key):
        lev[k] = [float(d[i].split()[ik]) for i in range(4, 4 + ntot)]
    # Second block LAYER
    # get the head of the table
    key = [x.split('(')[0] for x in d[4 + ntot + 1].strip()[1:-1].split()]
    lay = {}
    for ik, k in enumerate(key):
        lay[k] = [float(d[i].split()[ik]) for i in range(4 + ntot + 2, 4 + ntot + ntot + 1)]

    ##Third block CGASA LAYER
    ncgs = find_int(d[4 + ntot + ntot + 2])
    # get the head of the table
    key = [x.split('(')[0] for x in d[4 + ntot + ntot + 3].strip()[1:-1].split()]
    cgs = {}
    for ik, k in enumerate(key):
        cgs[k] = [float(d[i].split()[ik]) for i in range(4 + ntot + ntot + 4, 4 + ntot + ntot + 4 + ncgs)]
    return {'lev': lev, 'lay': lay, 'cgs': cgs, 'ntot': ntot, 'natm': natm}


@deco
def read_vtp(vtp_file):
    '''
    you may add your own vtp reader here!
    '''
    return read_vtp_momo(vtp_file)


def read_cgs_simple(kbin_input):
    '''
    This is a simple ncdf reader for
    the high resolution optical thickness file
    calculated with cgasa.
    You may tinker your own reader for your application.
    However: the output must be consistent!!!!!:
        {'lay_opt':float[n_wvl,n_lay,n_constituents]
         'tot_opt':float[n_wvl,n_constituents],
         'wvl':float[n_wvl] # in nm
         'seq':str_list[n_constituents]}
         'mgas_seq':str_list[all gases summarised in GAS]}
    '''

    try:
        with Dataset(kbin_input['FILES']['cgasa_file']) as ds:
            lay_opt = ds.groups['cgasa'].variables['lay_opt'][:] * 1.  # layer optical thickness
            toa_opt = ds.groups['cgasa'].variables['toa_opt'][:] * 1.  # total atmosphere optical thickness
            wvl = ds.groups['cgasa'].variables['wvl'][:] *1000.  # center wavelength of high resolution
            # the meaning of elements of last dimension of lay_opt and tot_opt
            seq = [s.strip() for s in ds.groups['cgasa'].seq.split(',')]
            mgas_seq = [s.strip() for s in ds.groups['cgasa'].mgas_seq.split(',')]

    except Exception as eee:
        kbin_deco.problem_print(eee)
        sys.exit(128)

    # check for negative optical thicknes (wherever it comes from)
    # but we dont wont it
    optok = True
    for iii, gas in enumerate(seq):
        for jjj in range(lay_opt.shape[1]):
            if lay_opt[:,jjj,iii].min() < 0:
                optok = False
                eee = 'Found negative optical thickness in layer %i of gas %s.\n'%(jjj,gas)
                eee += 'Kbin ignores it, since it is unphysical. You may check it ...'
                kbin_deco.problem_print(eee)
    if optok is False:
        lay_opt = lay_opt.clip(0.)
        toa_opt = toa_opt.clip(0.)


    # TODO:
    # Treat self broadening  correctly, in particular
    # the amf dependency squared,
    if kbin_input['INTERNALS']['self_cont'] is True:
        idx_h2o = where_in_list('H2O',seq)
        idx_slf = where_in_list('H2O.slf',seq)
        if (len(idx_h2o) > 0) and (len(idx_slf) > 0):
            idx_h2o, idx_slf = idx_h2o[0], idx_slf[0]
            toa_opt[:, idx_h2o] += toa_opt[:, idx_slf]
            lay_opt[:, :, idx_h2o] += lay_opt[:, :, idx_slf]
            seq.pop(idx_slf)
            toa_opt = np.delete(toa_opt,idx_slf,axis=1)
            lay_opt = np.delete(lay_opt, idx_slf, axis=2)
    if kbin_input['INTERNALS']['frgn_cont'] is True:
        idx_h2o = where_in_list('H2O',seq)
        idx_fgn = where_in_list('H2O.fgn',seq)
        if (len(idx_h2o) > 0) and (len(idx_fgn) > 0):
            idx_h2o, idx_fgn = idx_h2o[0], idx_fgn[0]
            toa_opt[:, idx_h2o] += toa_opt[:, idx_fgn]
            lay_opt[:, :, idx_h2o] += lay_opt[:, :, idx_fgn]
            seq.pop(idx_fgn)
            toa_opt = np.delete(toa_opt,idx_fgn,axis=1)
            lay_opt = np.delete(lay_opt, idx_fgn, axis=2)

    return {'lay_opt': lay_opt, 'toa_opt': toa_opt, 'wvl': wvl, 'seq': seq, 'mgas_seq': mgas_seq}

def where_in_list(idem,lisd):
    return [i for i in range(len(lisd)) if lisd[i] == idem]


# @deco
def read_cgs_momo(kbin_input):
    '''
    This is a very specific ncdf reader for 
    the high resolution optical thickness file in the 'lay_opt'
    !!!OLD!!! momo universe, calculated with !!!OLD!!!cgasa.


    '''
    n_constituents = 3
    # kbin_input['FILES']['cgs_file']
    try:
        with Dataset(kbin_input['FILES']['cgasa_file']) as ds:
            lay_opt = ds.variables['OPTKBIN'][:] * 1.  # layer optical thickness
            toa_opt = ds.variables['ATMOKBIN'][:] * 1.  # total atmosphere optical thickness
            wvl = ds.variables['LONDE'][:] * 1000.  # center wavelength of high resolution
            # optical thickness, unit: um
            # converted to nm!
            # the meaning of five elements of last dimension of lay_opt and tot_opt
            seq = ['H2O', 'MGAS', 'O3', 'h2o_frgn_cont', 'h2o_self_cont']

    except Exception as eee:
        kbin_deco.problem_print(eee)
        sys.exit(128)

    # this reader is defined for three constituents
    # can be programed more general later ...
    # [n_wvl,n_lay,n_constituents]
    lay_opt_out = lay_opt[:, :, 0:n_constituents] + 0.
    toa_opt_out = toa_opt[:, 0:n_constituents] + 0.

    # TODO:
    # Treat self broadening  correctly, in particular
    # the amf dependency squared,
    if kbin_input['INTERNALS']['self_cont'] is True:
        toa_opt_out[:, 0] += toa_opt[:, 4]
        lay_opt_out[:, :, 0] += lay_opt[:, :, 4]
    if kbin_input['INTERNALS']['frgn_cont'] is True:
        toa_opt_out[:, 0] += toa_opt[:, 3]
        lay_opt_out[:, :, 0] += lay_opt[:, :, 3]

    return {'lay_opt': lay_opt_out, 'toa_opt': toa_opt_out, 'wvl': wvl, 'seq': seq[0:n_constituents]}


def read_cgs(kbin_input):
    '''
    you may add your own high resolution 
    optical thickness reader here!'lay_opt'
    '''
    #return read_cgs_momo(kbin_input)
    return read_cgs_simple(kbin_input)


def read_rsr(rsr_idn):
    '''
    you may add your own rsr reader here!
    '''
    try:
        dum = rsr_idn.split(':')
    except Exception as eee:
        kbin_deco.problem_print(eee)
        print('The rsr entry must be of the form type:description (see example)')
        sys.exit(128)

    rsr_typ, rsr_dsc = dum[0], dum[1:]

    if rsr_typ == 'file':
        return read_rsr_momo(rsr_dsc[0])
    elif rsr_typ == 'gaussian':
        return create_gaussian_rsr(rsr_dsc)
    elif rsr_typ in ['rectangular', 'broadband']:
        return create_broadband_rsr(rsr_dsc)
    elif rsr_typ == 'none':
        return create_none_rsr(rsr_dsc)
    else:
        kbin_deco.problem_print('Can not identify rsr type from  %s' % rsr_idn)
        sys.exit(128)

def read_rsr_new(rsr_typ, rsr_dsc):
    '''

        center = float(rsr_dsc[0])
        fwhm = float(rsr_dsc[1])    '''
    # try:
    #     dum = rsr_idn.split(':')
    # except Exception as eee:
    #     kbin_deco.problem_print(eee)
    #     print('The rsr entry must be of the form type:description (see example)')
    #     sys.exit(128)

    # rsr_typ, rsr_dsc = dum[0], dum[1:]

    if rsr_typ == 'file':
        return read_rsr_momo(rsr_dsc[0])
    elif rsr_typ == 'gaussian':
        return create_gaussian_rsr(rsr_dsc)
    elif rsr_typ in ['rectangular', 'broadband']:
        return create_broadband_rsr(rsr_dsc)
    elif rsr_typ == 'none':
        return create_none_rsr(rsr_dsc)
    else:
        kbin_deco.problem_print('Can not identify rsr type from  %s' % rsr_idn)
        sys.exit(128)


@deco
# added by nils for CKDMIP propose
def create_none_rsr(rsr_dsc):
    try:
        min_wvl = float(rsr_dsc[0])
        max_wvl = float(rsr_dsc[1])
    except Exception as eee:
        kbin_deco.problem_print(eee)
        print('Can not identify min and max wvl from %s' % rsr_dsc)
        sys.exit(128)

    wvl = np.array((min_wvl, min_wvl, max_wvl, max_wvl))
    rsr = np.array((0., 1. / (max_wvl - min_wvl), 1. / (max_wvl - min_wvl), 0.))
    return {'wvl': wvl, 'rsr': rsr}

def create_broadband_rsr(rsr_dsc):
    try:
        min_wvl = float(rsr_dsc[0])
        max_wvl = float(rsr_dsc[1])
    except Exception as eee:
        kbin_deco.problem_print(eee)
        print('Can not identify min and max wvl from %s' % rsr_dsc)
        sys.exit(128)

    eps = (max_wvl - min_wvl) / 100000.
    wvl = np.array((min_wvl - eps, min_wvl, max_wvl, max_wvl + eps))
    rsr = np.array((0., 1. / (max_wvl - min_wvl), 1. / (max_wvl - min_wvl), 0.))
    return {'wvl': wvl, 'rsr': rsr}


@deco
def create_gaussian_rsr(rsr_dsc):
    try:
        center = float(rsr_dsc[0])
        fwhm = float(rsr_dsc[1])
    except Exception as eee:
        kbin_deco.problem_print(eee)
        print('Can not identify centre wvl and fwhm from %s' % rsr_dsc)
        sys.exit(128)
    sigma = fwhm / np.sqrt(np.log(256.))
    wvl = np.linspace(center - 4. * sigma, center + 4. * sigma, 100)
    rsr = 1. / sigma / np.sqrt(2. * np.pi) * np.exp((-(wvl - center) ** 2) / (2 * sigma ** 2))
    return {'wvl': wvl, 'rsr': rsr}


@deco
def read_rsr_momo(rsr_file):
    '''
    This is a very simple acii reader for 
    the file describing the relative spectral response in the 
    momo universe. You may write  your own reader for your
    application. However: the output must be consistent!!!!!
    wavelength in nm!!!!
    rsr dont need to be normalized. We do it internally!
    minmax of rsr wavelengths must be within minmax of 
    cgasa ot file!
    '''
    try:
        with open(rsr_file) as fp:
            d = fp.readlines()
    except Exception as eee:
        kbin_deco.problem_print(eee)
        sys.exit(128)
    d = [l for l in d if ((l[0] != '#') & (len(l.split()) == 2))]
    wvl = np.array([float(l.split()[0]) for l in d])
    rsr = np.array([float(l.split()[1]) for l in d])
    return {'wvl': wvl, 'rsr': rsr}


def read_sol(solar_file):
    '''
    This is a very simple acii reader for
    solar constant. You may write  your own reader for your
    aplication. However: the output must be consistent!!!!!
    wavelength in nm!!!!
    Unit of sol  should be power / (wavelength m2)
    minmax of sol wavelengths must be outside minmax of 
    rsr !
    '''
    try:
        with open(solar_file) as fp:
            d = fp.readlines()
    except Exception as eee:
        kbin_deco.problem_print(eee)
        sys.exit(128)
    d = [l for l in d if ((l[0] != '#') & (len(l.split()) == 2))]
    wvl = np.array([float(l.split()[0]) for l in d])
    sol = np.array([float(l.split()[1]) for l in d])
    return {'wvl': wvl, 'sol': sol}


def create_fake_sol(rsr):
    return {'wvl': rsr['wvl'] + 0., 'sol': rsr['rsr'] * 0. + 1}


@deco
def kb_read_data(kbin_input):
    '''
    reads all data from relevan files in kbin input
    and returns a dict containing all data
    
    '''
    # 1. read cgs data
    cgs = read_cgs(kbin_input)
    # print (cgs['lay_opt'].shape)

    # 2. read vertical profile
    vtp = read_vtp(kbin_input['FILES']['vtp_file'])
    # print (vtp['cgs']['Press'])

    # 3. read relative spectral response
    rsr = read_rsr(kbin_input['FILES']['rsr_identifier'])

    # 4. read solar data
    if kbin_input['FILES']['solar_file'] == 'NOT_USED':
        sol = create_fake_sol(rsr)
    else:
        sol = read_sol(kbin_input['FILES']['solar_file'])

    kbin_data = {'cgs': cgs, 'vtp': vtp, 'rsr': rsr, 'sol': sol, 'kb': {}}

    # Create other needed stuff
    kbin_data['kb']['min_wvl'] = kbin_data['rsr']['wvl'].min()
    kbin_data['kb']['max_wvl'] = kbin_data['rsr']['wvl'].max()
    kbin_data['kb']['n_lay'] = kbin_data['cgs']['lay_opt'].shape[1]
    kbin_data['kb']['lay'] = np.arange(kbin_data['kb']['n_lay'])
    # kbin_data['kb']['n_constituents'] = kbin_data['cgs']['lay_opt'].shape[2]
    # kbin_data['kb']['constituents'] = np.arange(kbin_data['kb']['n_constituents'])

    return kbin_data


def write_vtp_momo(ds, kbin_data, kbin_input):
    '''
    This is a very very specific write for 
    the the vertical profile information in 
    the momo universe. You may write  your own 
    write for your aplication. 
    '''
    ds.source = (kbin_input['FILES']['vtp_file'])
    ds.createDimension('ntot_lev', kbin_data['vtp']['ntot'])
    ds.createDimension('natm_lev', kbin_data['vtp']['natm'])
    ds.createDimension('ntot_lay', kbin_data['vtp']['ntot'] - 1)
    ds.createDimension('natm_lay', kbin_data['vtp']['natm'] - 1)
    dims = {'lev': 'ntot_lev', 'lay': 'ntot_lay', 'cgs': 'natm_lay'}
    for element in dims:
        grp = ds.createGroup(element)
        for key in kbin_data['vtp'][element]:
            grp.createVariable(key, float, (dims[element],))
            grp.variables[key][:] = kbin_data['vtp'][element][key]


@deco
def kb_write_data_ori(kbin_data, kbin_input):
    '''
    write all data to output file
    '''
    with Dataset(kbin_input['FILES']['output_file'], 'w', clobber=True) as ds:
        # 1. All general attributes
        ds.General = 'This contains result of k-binning ... more things to write'
        ds.Author = 'Rene Preusker and Juergen Fischer'
        ds.version = about_me()['version']
        ds.User = getpass.getuser()
        ds.CreationDate = time.asctime()

        # 2. All Input into attributes:
        # __setattr__(str(atr),dct['attributes'][atr])
        ds.createGroup('input')
        for element in kbin_input:
            dum = ds.groups['input'].createGroup(element)
            for key in kbin_input[element]:
                dum.__setattr__(str(key), str(kbin_input[element][key]))

        # 3. All cgasa
        if kbin_input['FILES']['detailed_output'] is True:
            ds.createGroup('cgs')
            ds.groups['cgs'].seq = ', '.join(kbin_data['cgs']['seq'])
            ds.groups['cgs'].mgas_seq = ', '.join(kbin_data['cgs']['mgas_seq'])
            ds.groups['cgs'].source = (kbin_input['FILES']['cgasa_file'])
            ds.groups['cgs'].createDimension('wvl', kbin_data['cgs']['lay_opt'].shape[0])
            ds.groups['cgs'].createDimension('layer', kbin_data['cgs']['lay_opt'].shape[1])
            ds.groups['cgs'].createDimension('constituents', kbin_data['cgs']['lay_opt'].shape[2])
            ds.groups['cgs'].createVariable('lay_opt', float, ('wvl', 'layer', 'constituents'))
            ds.groups['cgs'].createVariable('toa_opt', float, ('wvl', 'constituents'))
            ds.groups['cgs'].createVariable('wvl', float, ('wvl',))

            for key in ('toa_opt', 'lay_opt', 'wvl'):
                ds.groups['cgs'].variables[key][:] = kbin_data['cgs'][key]

        # 4. All vtp (quite specific ....)
        ds.createGroup('vtp')
        write_vtp_momo(ds.groups['vtp'], kbin_data, kbin_input)

        # 5. All rsr
        if kbin_input['FILES']['detailed_output'] is True:
            ds.createGroup('rsr')
            ds.groups['rsr'].source = (kbin_input['FILES']['rsr_identifier'])
            ds.groups['rsr'].createDimension('wvl', len(kbin_data['rsr']['wvl']))
            for key in ('wvl', 'rsr'):
                ds.groups['rsr'].createVariable(key, float, ('wvl',))
                ds.groups['rsr'].variables[key][:] = kbin_data['rsr'][key]

        # 6. All solar
        if kbin_input['FILES']['detailed_output'] is True:
            ds.createGroup('sol')
            ds.groups['sol'].source = (kbin_input['FILES']['solar_file'])
            ds.groups['sol'].createDimension('wvl', len(kbin_data['sol']['wvl']))
            for key in ('wvl', 'sol'):
                ds.groups['sol'].createVariable(key, float, ('wvl',))
                ds.groups['sol'].variables[key][:] = kbin_data['sol'][key]

        # 6. All kbin
        ds.createGroup('kb')
        ds.groups['kb'].seq = ', '.join(kbin_data['cgs']['seq'])
        for key in ('wvl', 'lay', 'amf', 'weight'):
            ds.groups['kb'].createDimension(key, (kbin_data['kb']['n_' + key]))
            ds.groups['kb'].createVariable(key, float, (key,))
            ds.groups['kb'].variables[key][:] = kbin_data['kb'][key]
        ds.groups['kb'].createVariable('mapping', float, ('wvl',))
        ds.groups['kb'].variables['mapping'][:] = kbin_data['kb']['mapping']

        itr =  ('rsr', ('wvl',)), ('norm_rsr', ('wvl',))
        for key, dims in itr:
            ds.groups['kb'].createVariable(key, float, dims)
            ds.groups['kb'].variables[key][:] = kbin_data['kb'][key]
        
        if kbin_input['FILES']['detailed_output'] is True:
            itr = KBINOUT_FULL
        else:
            itr = KBINOUT_SMALL
        for ggg in (kbin_data['cgs']['seq'] + ['tot']):
            for key, dims, lname in itr:
                key = key + '_' + ggg
                lname = lname + '_' + ggg
                ds.groups['kb'].createVariable(key, float, dims)
                ds.groups['kb'].variables[key].__setattr__('long_name',lname)
                ds.groups['kb'].variables[key][:] = kbin_data['kb'][key]

        # print (kbin_data['kb']['toa_ave_trans_h2o'][:])
        # print (kbin_data['kb']['toa_trans_h2o'][:])
        # print (kbin_data['kb']['lay_ave_trans_tot'][-3,:])
        # print (kbin_data['kb']['lay_trans_tot'][-3,:])
#@deco
def kb_write_data(kbin_data, kbin_input):
    '''
    write all data to output file
    '''
    with Dataset(kbin_input['FILES']['output_file'], 'w', clobber=True) as ds:
        # 1. All general attributes
        ds.General = 'This contains result of k-binning ... more things to write'
        ds.Author = 'Rene Preusker and Juergen Fischer'
        ds.version = about_me()['version']
        ds.User = getpass.getuser()
        ds.CreationDate = time.asctime()

        # 2. All Input into attributes:
        # __setattr__(str(atr),dct['attributes'][atr])
        ds.createGroup('input')
        for element in kbin_input:
            dum = ds.groups['input'].createGroup(element)
            for key in kbin_input[element]:
                dum.__setattr__(str(key), str(kbin_input[element][key]))

        # 3. All cgasa
        if kbin_input['FILES']['detailed_output'] is True:
            ds.createGroup('cgs')
            ds.groups['cgs'].seq = ', '.join(kbin_data['cgs']['seq'])
            ds.groups['cgs'].mgas_seq = ', '.join(kbin_data['cgs']['mgas_seq'])
            ds.groups['cgs'].source = (kbin_input['FILES']['cgasa_file'])
            ds.groups['cgs'].createDimension('wvl', kbin_data['cgs']['lay_opt'].shape[0])
            ds.groups['cgs'].createDimension('layer', kbin_data['cgs']['lay_opt'].shape[1])
            ds.groups['cgs'].createDimension('constituents', kbin_data['cgs']['lay_opt'].shape[2])
            ds.groups['cgs'].createVariable('lay_opt', float, ('wvl', 'layer', 'constituents'))
            ds.groups['cgs'].createVariable('toa_opt', float, ('wvl', 'constituents'))
            ds.groups['cgs'].createVariable('wvl', float, ('wvl',))

            for key in ('toa_opt', 'lay_opt', 'wvl'):
                ds.groups['cgs'].variables[key][:] = kbin_data['cgs'][key]
        # 
        # # 4. All vtp (quite specific ....)
        # ds.createGroup('vtp')
        # write_vtp_momo(ds.groups['vtp'], kbin_data, kbin_input)

        # 5. All rsr
        if kbin_input['FILES']['detailed_output'] is True:
            ds.createGroup('rsr')
            ds.groups['rsr'].source = (kbin_input['FILES']['rsr_identifier'])
            ds.groups['rsr'].createDimension('wvl', len(kbin_data['rsr']['wvl']))
            for key in ('wvl', 'rsr'):
                ds.groups['rsr'].createVariable(key, float, ('wvl',))
                ds.groups['rsr'].variables[key][:] = kbin_data['rsr'][key]

        # 6. All solar
        if kbin_input['FILES']['detailed_output'] is True:
            ds.createGroup('sol')
            ds.groups['sol'].source = (kbin_input['FILES']['solar_file'])
            ds.groups['sol'].createDimension('wvl', len(kbin_data['sol']['wvl']))
            for key in ('wvl', 'sol'):
                ds.groups['sol'].createVariable(key, float, ('wvl',))
                ds.groups['sol'].variables[key][:] = kbin_data['sol'][key]

        # 6. All kbin
        ds.createGroup('kb')
        ds.groups['kb'].seq = ', '.join(kbin_data['cgs']['seq'])
        for key in ('wvl', 'lay', 'amf', 'weight'):
            ds.groups['kb'].createDimension(key, (kbin_data['kb']['n_' + key]))
            ds.groups['kb'].createVariable(key, float, (key,))
            ds.groups['kb'].variables[key][:] = kbin_data['kb'][key]
        ds.groups['kb'].createVariable('mapping', float, ('wvl',))
        ds.groups['kb'].variables['mapping'][:] = kbin_data['kb']['mapping']

        itr =  ('rsr', ('wvl',)), ('norm_rsr', ('wvl',))
        for key, dims in itr:
            ds.groups['kb'].createVariable(key, float, dims)
            ds.groups['kb'].variables[key][:] = kbin_data['kb'][key]
        
        if kbin_input['FILES']['detailed_output'] is True:
            itr = KBINOUT_FULL
        else:
            itr = KBINOUT_SMALL
        for ggg in (kbin_data['cgs']['seq'] + ['tot']):
            for key, dims, lname in itr:
                key = key + '_' + ggg
                lname = lname + '_' + ggg
                ds.groups['kb'].createVariable(key, float, dims)
                ds.groups['kb'].variables[key].__setattr__('long_name',lname)
                ds.groups['kb'].variables[key][:] = kbin_data['kb'][key]

        # print (kbin_data['kb']['toa_ave_trans_h2o'][:])
        # print (kbin_data['kb']['toa_trans_h2o'][:])
        # print (kbin_data['kb']['lay_ave_trans_tot'][-3,:])
        # print (kbin_data['kb']['lay_trans_tot'][-3,:])
#@deco
def k2m_read_data(kbin_input):
    try:
        d = simple_ncdf2dict(kbin_input['FILES']['kbin_file'])
    except Exception as eee:
        kbin_deco.problem_print(eee)
        sys.exit(128)
    d['kb']['n_lay'] = d['kb']['lay'].size
    d['kb']['n_weight'] = d['kb']['weight'].size
    d['kb']['seq'] = [_.strip() for _ in d['kb']['attributes']['seq'].split(',')]
    return d

def k2m_write_kb_file(kbin_data, k2m_input):
    for igas, gas in enumerate(kbin_data['kb']['seq']):
        wgt = kbin_data['kb']['weight']
        ext = kbin_data['kb']['lay_ave_opt_%s'%gas]
        scl = k2m_input['SCALE']['scale_%s'%gas]
        msg = 'kbin out file for %s. '%gas
        vtp = kbin_data['vtp']['cgs']
        prs = vtp['Press']

        name = k2m_input['FILES']['output_file_base'] + '.%s.kb' % gas
        if gas == 'H2O':
            # convert to kg/m2 per layer
            amo_typ = [gas]
            amo_unt = 'kg / m2'
            amo_mss = [vtp['H2O'] * 18.015  / AVOGADRO /1000.]
            amo_mss *= scl
        elif gas == 'O3':
            amo_typ = [gas]
            amo_unt = 'DU'
            amo_mss = [vtp['O3'] / 2.69e20 ]
            amo_mss *= scl
        elif gas == 'MGAS':
            amo_typ = [g for g in vtp if g not in ['attributes','Lay','Press','Tref','dz','H2O','O3','air']]
            amo_unt = '1/m2'
            amo_mss = [vtp[rrr]*scl for rrr in amo_typ]
        else:
            amo_typ = [gas]
            amo_unt = '1/m2'
            amo_mss *= scl

        out = []
        out.append(msg)
        out.append('')
        out.append('        wvl1        wvl2   nbins  nlay')
        out.append('%12.4f%12.4f%8i%6i'%(
                        kbin_data['kb']['wvl'].min(),
                        kbin_data['kb']['wvl'].max(),
                        kbin_data['kb']['n_weight'],
                        kbin_data['kb']['n_lay']))
        out.append('')
        out.append('bin weights:')
        out.append(''.join(['%13.6e'%w for w in wgt]))
        out.append('')
        out.append('   Press(Pa)  <- bins  k-coefficients .......  ->')
        for ilay,iprs in enumerate(prs):
            #(3x,e9.3,1x,'+string(fix(ic))+'(e13.6)
            lin = '   %9.3e '%iprs
            lin += ''.join(['%13.6e'%e for e in ext[:,ilay]*scl[ilay]])
            out.append(lin)
        out.append('')
        out.append('')
        out.append('relevant Masses [%s]:'%amo_unt)
        for ityp,typ in enumerate(amo_typ):
            lin = '%3s lay, tot:'%typ
            lin += ''.join(['%13.6e' % m for m in amo_mss[ityp]])
            lin += '%13.6e'% amo_mss[ityp].sum()
            out.append(lin)


        with open(name,'w') as fp:
            for o in out:
                fp.write(o+'\n')


def about_me():
    dct = {}
    libname = inspect.stack()[0][1]
    dct['scriptname'] = inspect.stack()[1][1]
    dct['scriptdir'] = os.path.realpath(os.path.abspath(os.path.split(dct['scriptname'])[0]))
    dct['libdir'] = os.path.realpath(os.path.abspath(os.path.split(libname)[0]))
    #vers_file = os.path.join(dct['libdir'], '..', 'current_version.txt')
    vers_file = os.path.join(dct['libdir'], '..', '..', 'current_version.txt')
    dct['version'] = open(vers_file).readlines()[0][:-1]
    dct['cwd'] = os.path.abspath(os.getcwd())
    dct['date'] = time.ctime()
    return dct
