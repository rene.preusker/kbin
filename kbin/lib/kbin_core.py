import sys
import numpy as np
from . import kbin_deco
from . import kbin_tools
from . import kbin_sort 
# import kbin_deco
# import kbin_tools
# import kbin_sort 

deco = kbin_deco.time_decorator
#deco = kbin_deco.debug_decorator
#deco = kbin_deco.none_decorator

# changed by nils 14.2.2021
# kbin_data['kb']['n_wvl']
#kbin_data['kb']['wvl']

@deco
def prepare_data(kbin_data, kbin_input):
    '''
    prepare the kbinning:
    1. reduce high resolution cgasa to minmax of rsr
    2. interpolate and normalize rsr to high resolution of cgasa
    3. calc amf and transmissions

    TODO:
    Treat H2O self broadening  correctly, in particular
    the amf dependency squared,
    (But priority is low (3 orders of magnitude in opt))

    !!!
    kbin_data is changed inplace!
    !!!
    '''
    
    #Check wvl overlaps and raise error if not!
    if kbin_data['kb']['min_wvl'] < kbin_data['cgs']['wvl'].min():
        eee=(('Error: Minimum of rsr (%f nm) is smaler than minimum of \n'+
             'high resolution extinction data (%f nm)! Exiting') 
             % (kbin_data['kb']['min_wvl'],kbin_data['cgs']['wvl'].min()) )
        kbin_deco.problem_print(eee)
        sys.exit(128)
        
    if kbin_data['kb']['max_wvl'] > kbin_data['cgs']['wvl'].max():
        eee=(('Error: Maximum of rsr (%f nm) is larger than maximum of \n'+
             'high resolution extinction data (%f nm)! Exiting') 
             % (kbin_data['kb']['max_wvl'],kbin_data['cgs']['wvl'].max()) )
        kbin_deco.problem_print(eee)
        sys.exit(128)
        
    if kbin_data['kb']['min_wvl'] < kbin_data['sol']['wvl'].min():
        eee=(('Error: Minimum of rsr (%f nm) is smaler than minimum of \n'+
             'solar data (%f nm)! Exiting') 
             % (kbin_data['kb']['min_wvl'],kbin_data['sol']['wvl'].min()) )
        kbin_deco.problem_print(eee)
        sys.exit(128)

    if kbin_data['kb']['max_wvl'] > kbin_data['sol']['wvl'].max():
        eee=(('Error: Maximum of rsr (%f nm) is larger than maximum of \n'+
             'solar data (%f nm)! Exiting') 
             % (kbin_data['kb']['max_wvl'],kbin_data['sol']['wvl'].max()) )
        kbin_deco.problem_print(eee)
        sys.exit(128)

        
        
    #     
    if kbin_input['CONFIGURATION']['max_err_lay'].size == 1:
        kbin_input['CONFIGURATION']['max_err_lay']=np.zeros(kbin_data['kb']['n_lay'])+kbin_input['CONFIGURATION']['max_err_lay'][0]
    if kbin_input['CONFIGURATION']['max_err_lay'].size != kbin_data['kb']['n_lay']:
        eee=(('Error: "max_err_lay" should have either 1 or %i elements.'
              % kbin_data['kb']['n_lay']) )
        kbin_deco.problem_print(eee)
        sys.exit(128)
        
    
    #Bring all to a common wavelength scale
    mima_idx = np.searchsorted(kbin_data['cgs']['wvl']                              # NOTE uncommented by nils
                         ,np.array([kbin_data['kb']['min_wvl'],kbin_data['kb']['max_wvl']]))
    kbin_data['kb']['n_wvl'] = mima_idx[1]-mima_idx[0]-2                             # uncommented by nils
    kbin_data['kb']['wvl'] = np.linspace(kbin_data['cgs']['wvl'][mima_idx[0]+1]      # uncommented by nils
                                       , kbin_data['cgs']['wvl'][mima_idx[1]-1]
                                       , kbin_data['kb']['n_wvl'])
    # kbin_data['kb']['n_wvl'] =kbin_data['cgs']['wvl'][:].shape[0]
    # #kbin_deco.problem_print(((mima_idx[1]-mima_idx[0])))
    # kbin_data['kb']['wvl'] = np.linspace(kbin_data['cgs']['wvl'][0]
    #                                    , kbin_data['cgs']['wvl'][-1]
    #                                    , kbin_data['kb']['n_wvl'])

    
    
    #kbin_data['kb']['wvl'] = kbin_data['cgs']['wvl'][mima_idx[0]+1:mima_idx[1]-1] + 0.
    #kbin_data['kb']['n_wvl'] = len(kbin_data['kb']['wvl'])

    
    lay_opt = kbin_data['cgs']['lay_opt'][mima_idx[0]+1:mima_idx[1]-1,:,:]
    toa_opt = kbin_data['cgs']['toa_opt'][mima_idx[0]+1:mima_idx[1]-1,:]

    lay_opt = np.zeros((kbin_data['kb']['n_wvl'],kbin_data['kb']['n_lay'],len(kbin_data['cgs']['seq'])))
    for i_lay in range(kbin_data['kb']['n_lay']):
        for i_gas in range(len(kbin_data['cgs']['seq'])):
            lay_opt[:, i_lay, i_gas] = np.interp(kbin_data['kb']['wvl'],
                                                 kbin_data['cgs']['wvl'],
                                                 kbin_data['cgs']['lay_opt'][:, i_lay, i_gas])

    toa_opt = lay_opt.sum(axis=1)
    
    #
    #kbin_data['kb']['wvl']=kbin_data['kb']['wvl']
    kbin_data['kb']['rsr']=np.interp(kbin_data['kb']['wvl']
                                         ,kbin_data['rsr']['wvl'],kbin_data['rsr']['rsr'])
    
    # modify rsr to account for solar
    kbin_data['kb']['rsr'] *= np.interp(kbin_data['kb']['wvl']
                                         ,kbin_data['sol']['wvl'],kbin_data['sol']['sol'])
    
    # finaly normalize it
    norm = np.trapz(kbin_data['kb']['rsr'],kbin_data['kb']['wvl'])
    kbin_data['kb']['norm_rsr'] = kbin_data['kb']['rsr']/norm
    
    
    #3.a Define amf vector 
    kbin_data['kb']['amf'] = np.linspace(kbin_input['INTERNALS']['min_amf']
                                 ,kbin_input['INTERNALS']['max_amf']
                                 ,kbin_input['INTERNALS']['nnn_amf']) 
    kbin_data['kb']['n_amf'] = len(kbin_data['kb']['amf'])
    
    
    # 3.b Total optical thickness
    # toa: total atmosphere
    # lay: in layer
    # ttl: from top to layer 
    for igas,gas in enumerate(kbin_data['cgs']['seq']):
        kbin_data['kb']['toa_opt_%s'%gas] = toa_opt[:,igas] + 0.
        kbin_data['kb']['lay_opt_%s'%gas] = lay_opt[:,:,igas] + 0.
        kbin_data['kb']['ttl_opt_%s'%gas] = lay_opt[:,:,igas].cumsum(axis=1)
    
    
    
    for key in ('toa','lay','ttl'):
        key='%s_opt_'%key
        gas = kbin_data['cgs']['seq'][0]
        kbin_data['kb'][key+'tot'] = kbin_data['kb'][key+gas] + 0. 
        for gas in kbin_data['cgs']['seq'][1:]:
            kbin_data['kb'][key+'tot'] += kbin_data['kb'][key+gas]
                                
            
    # 4.b transmission total (TOA -> BOA):
    for ggg in  (kbin_data['cgs']['seq']+['tot']):
        nam_opt = 'toa_opt_%s'%ggg
        nam_tra = 'toa_trans_%s'%ggg
        kbin_data['kb'][nam_tra] = np.zeros_like(kbin_data['kb']['amf'])
        for iamf,amf in enumerate(kbin_data['kb']['amf']):
            kbin_data['kb'][nam_tra][iamf] = kbin_tools.opt_to_trans(kbin_data['kb'][nam_opt]
                                                        ,kbin_data['kb']['norm_rsr']
                                                        ,kbin_data['kb']['wvl']
                                                        ,amf)
    
    # 4.c transmission  per layer and from top to layer:
    for ggg in (kbin_data['cgs']['seq']+['tot']):
        for key in ('lay','ttl'):
            nam_opt = '%s_opt_%s'%(key,ggg)
            nam_tra = '%s_trans_%s'%(key,ggg)
            kbin_data['kb'][nam_tra] = np.zeros((kbin_data['kb']['n_lay'],kbin_data['kb']['n_amf']))
            for ilay in range(kbin_data['kb']['n_lay']):
                for iamf,amf in enumerate(kbin_data['kb']['amf']):
                    kbin_data['kb'][nam_tra][ilay,iamf] = kbin_tools.opt_to_trans(
                                                            kbin_data['kb'][nam_opt][:,ilay]
                                                            ,kbin_data['kb']['norm_rsr']
                                                            ,kbin_data['kb']['wvl']
                                                            ,amf)
    return True

    
@deco
def mk_kbin(kbin_data, kbin_input):
    '''
    !!!
    kbin_data is changed inplace!
    !!!
    '''
    kb = kbin_data['kb']
    kbc = kbin_input['CONFIGURATION']
    kbi = kbin_input['INTERNALS']
    
    if kbi['kbin_method'] == 'mean':
        doit = kbin_sort.kbin_mean
    elif kbi['kbin_method'] == 'correlated_toa':
        doit  = kbin_sort.correlated_toa
    elif kbi['kbin_method'] == 'correlated_strongest_layer':
        doit  = kbin_sort.correlated_strongest_layer
    elif kbi['kbin_method'] == 'correlated_toa_strongest_layer':
        doit = kbin_sort.correlated_toa_strongest_layer
    elif kbi['kbin_method'] == 'uncorrelated':
        doit  = kbin_sort.uncorrelated
    else:
        eee='Error: Unknown kbinning method'
        kbin_deco.problem_print(eee)
        sys.exit(128)

    w,m = doit(kbin_data, kbin_input)
    
    kb['mapping'] = m
    kb['weight'] = w
    kb['n_weight'] = len(w)
    
    types = kbin_data['cgs']['seq']+['tot']
    kbin_tools.calc_all_ave_opt(kb,kbi['average_method'], types)
    kbin_tools.calc_all_ave_trans(kb,types)
    
    return True
    

