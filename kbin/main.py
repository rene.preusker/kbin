'''
Runner for kbin

usage: 
see examples in test dir

'''

import sys

from lib import kbin_io
from lib import kbin_core
from lib import kbin_deco
deco = kbin_deco.time_decorator

USAGE=__doc__
DESCR="KBIN" 


@deco
def main():

    if len(sys.argv) != 2:
        print(USAGE)
        sys.exit(128)

    input_file = sys.argv[1]
    kbin_input = kbin_io.kb_read_and_analyze_input(input_file)
    kbin_data = kbin_io.kb_read_data(kbin_input)
    
    # TODO: evaluate dum  
    dum = kbin_core.prepare_data(kbin_data,kbin_input)
    dum = kbin_core.mk_kbin(kbin_data,kbin_input)
    dum = kbin_io.kb_write_data(kbin_data,kbin_input)
    
     
        
        

if __name__=='__main__':
    main()
