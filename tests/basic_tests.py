'''
Basic testing of necessary modules and aux data ...
'''
from __future__ import print_function

import sys



print('   Testing python version:',sys.version_info, end='  ')
if (sys.version_info <= (3, 6, 0)) and (sys.version_info >= (3, 0, 0)):
    print('failed')
    print('Your python version seems to be old. Should work, but is not tested!')
elif (sys.version_info <= (2, 7, 13)) and (sys.version_info >= (2, 7, 0)):
    print('failed')
    print('Your python version seems to be old. Should work, but is not tested!')
elif (sys.version_info < (2, 7, 0)):
    print('failed')
    print('Your python version seems to be very old. Forget it!')
else:
    print('ok')


print('   Testing import of needed modules: ')
needed_modules=('numpy','netCDF4','termcolor','os')
for nm in needed_modules:
    try:
        print('    ',nm,end='  ')
        dum=__import__(nm)
        print('ok')
    except :
        print('failed')
        print('Module ',nm,' could not imported. You may have a problem!')


